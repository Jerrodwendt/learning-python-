subprocess.call('Windows Console.py', creationflags=subprocess.CREATE_NEW_CONSOLE)

#!/usr/bin/python3
username = 'Chris'

# generate passphrase
pw_length = 6
phrase = subprocess.check_output(['pwgen', str(pw_length), '1'])
phrase = phrase.decode('utf-8').strip()

dev_null = open('/dev/null', 'w')
passwd = subprocess.Popen(['sudo', 'passwd', user], stdin=subprocess.PIPE,
                          stdout=dev_null.fileno(),
                          stderr=subprocess.STDOUT)
passwd.communicate( ((phrase + '\n')*2).encode('utf-8') )
if passwd.returncode != 0:
    raise OSError('password setting failed')

bash-3.00# python ./pass2.py
phrase = subprocess.check_output(['pwgen', str(pw_length), '1'])
NameError: name 
